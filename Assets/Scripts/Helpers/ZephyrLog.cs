﻿using UnityEngine;

namespace Helpers
{
    public class ZephyrLog {

        private string _className;
        private LogPriorityType _priority;

        public ZephyrLog(string className, LogPriorityType priority)
        {
            _className = className;
            _priority = priority;
        }

        public void ReportingDebug(string message, string prefix)
        {
            if (_priority > LogPriorityType.Debug) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "DEBUG", message, prefix);
            Debug.Log(displayingMessage);
        }

        public void Info(string message, string prefix)
        {
            if (_priority > LogPriorityType.Info) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "INFO", message, prefix);
            Debug.Log(displayingMessage);
        }

        public void Warning(string message, string prefix)
        {
            if (_priority > LogPriorityType.Warning) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "WARNING", message, prefix);
            Debug.LogWarning(displayingMessage);
        }

        public void Error(string message, string prefix)
        {
            if (_priority > LogPriorityType.Error) return;

            string displayingMessage = string.Format("[{0} *-{1}-* {2}]", "ERROR", message, prefix);
            Debug.Log(displayingMessage);
        }
    }
}
