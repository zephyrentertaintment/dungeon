﻿namespace Helpers
{
    public enum LogPriorityType
    {
        Universal = 0,
        Debug = 5,
        Info = 10,
        Warning = 15,
        Error = 20,
        Disabled = 25
    }
}

