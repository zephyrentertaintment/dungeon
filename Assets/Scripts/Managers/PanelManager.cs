﻿using System.Collections.Generic;
using Models.Enums;
using Models.GUI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Managers
{
    public class PanelManager : SerializedMonoBehaviour
    {

        [SerializeField]
        public Dictionary<PanelType, PanelBase> Panels = new Dictionary<PanelType, PanelBase>();

        [HideInInspector]
        public PanelBase ActivePanel;
        
        public void OpenPanel(PanelType panelName)
        {
            if (ActivePanel != null)
                ActivePanel.gameObject.SetActive(false);

            PanelBase panel = null;

            if (Panels.TryGetValue(panelName, out panel))
            {
                ActivePanel = panel;
                panel.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("Panel not found.");
            }
        }

        
    }
}
