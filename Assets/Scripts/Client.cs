﻿using UnityEngine;

public class Client : MonoBehaviour {

    #region Singleton
    private static Client _instance;

    public static Client Instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            _instance = FindObjectOfType<Client>();
            if (_instance != null)
                return _instance;

            var obj = new GameObject("Client");
            _instance = obj.AddComponent<Client>();
            return _instance;
        }
    } 
    #endregion
}
