﻿using System;
using Controllers;
using Helpers;
using JetBrains.Annotations;
using Models.Enums;
using Models.GUI;
using UnityEngine.UI;

namespace Views.GUI
{
    public class CombatPanelView : PanelBase
    {
        public static Action<TurnType> TurnActionTaken;
        private ZephyrLog _logger = new ZephyrLog(typeof(CombatPanelView).Name, LogPriorityType.Debug);
        
        public Button Attack; 
        public Button Block;
        public Button Inventory;
        public Button Spells;
        public Button Escape;

        protected override void OnEnable()
        {
            TurnActionTaken += GameController.Instance.TurnBasedControllerInstance.ReceiveButtonAction;
        }

        protected override void OnDisable()
        {
            // ReSharper disable once DelegateSubtraction
            TurnActionTaken -= GameController.Instance.TurnBasedControllerInstance.ReceiveButtonAction;
        }

        [UsedImplicitly]
        public void ButtonClicked(int turnTypeValue) {
            if (turnTypeValue == 0)
            {
                _logger.ReportingDebug("Unknown Button. No Action has been taken.", "[Logic Error]");
                return;
            }

            TurnType turnType = (TurnType) turnTypeValue;
            TurnActionTaken(turnType);
            _logger.ReportingDebug(turnType.ToString(), "");
        }
    }

    
}
