﻿using Controllers;
using JetBrains.Annotations;
using Models.Enums;
using Models.GUI;
using UnityEngine;

namespace Views.GUI
{
    public class MainMenuPanelView : PanelBase
    {
        protected override void OnEnable()
        {
            base.OnEnable();
            Debug.Log(typeof(MainMenuPanelView).Name);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Debug.Log(typeof(MainMenuPanelView).Name);
        }

        [UsedImplicitly]
        public void OpenIdleClick()
        {
            GuiController.PanelManager.OpenPanel(PanelType.IdleClick);
        }

        [UsedImplicitly]
        public void OpenCombat()
        {
            GuiController.PanelManager.OpenPanel(PanelType.Combat);
        }

        [UsedImplicitly]
        public void OpenCharacterCreationPanel()
        {
            GuiController.PanelManager.OpenPanel(PanelType.CharacterCreation);
        }
    }
}
