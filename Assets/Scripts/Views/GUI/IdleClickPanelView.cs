﻿using Controllers;
using Models.GUI;
using Helpers;
using Models.Enums;

namespace Views.GUI
{
    public class IdleClickPanelView : PanelBase {

        ZephyrLog _log = new ZephyrLog(typeof(IdleClickPanelView).Name, LogPriorityType.Debug);

        protected override void OnEnable()
        {
            base.OnEnable();
            IdleClickController.Instance.StartCollecting();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        public void OnCloseButtonClicked()
        {
            GuiController.PanelManager.OpenPanel(PanelType.MainMenu);
        }
    }
}
