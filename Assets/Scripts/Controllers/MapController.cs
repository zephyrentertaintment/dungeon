﻿using Models.Objects;
using UnityEngine;

namespace Controllers
{
    public class MapController : MonoBehaviour
    {

        private int _minColumnSize = 50;
        private int _maxColumnSize = 200;
        private int _minRowSize = 50;
        private int _maxRowSize = 200;

        private int _minRooms = 5;
        private int _maxRooms = 20;
        private int _minRoomWidth = 5;
        private int _maxRoomWidth = 10;
        private int _minRoomHeight = 5;
        private int _maxRoomHeight = 10;
        private int _minCorridorLength = 6;
        private int _maxCorridorLength = 12;

        private int _columns;
        private int _rows;
        private int _numRooms;

        private Tile[][] _tileBoard;

        private void Awake()
        {
            SetNumbers();

            SetupTileBoard();
            
        }

        private void SetNumbers()
        {
            _columns = Random.Range(_minColumnSize, _maxColumnSize);
            _rows = Random.Range(_minRowSize, _maxRowSize);
            _numRooms = Random.Range(_minRooms, _maxRooms);
        }

        private void SetupTileBoard()
        {
            _tileBoard = new Tile[_columns][];

            for (int i = 0; i < _tileBoard.Length; i++)
            {
                _tileBoard[i] = new Tile[_rows];
            }
        }
    }
}
