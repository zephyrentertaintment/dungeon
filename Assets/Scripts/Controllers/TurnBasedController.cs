﻿using System.Collections.Generic;
using Helpers;
using Models.Character;
using Models.Enums;
using UnityEngine;
using Views.GUI;
using Random = System.Random;

namespace Controllers
{
    public class TurnBasedController : MonoBehaviour
    {
        private ZephyrLog _logger = new ZephyrLog(typeof(TurnBasedController).Name, LogPriorityType.Debug);
        private CharacterBase _model;
        private TurnBasedView _view;

        public void ReceiveButtonAction(TurnType turnType)
        {
            switch (turnType)
            {
                case TurnType.None:
                    _logger.Error("Unknown Turn Type. Please check TurnType to clarify type.", "");
                    break;
                case TurnType.Attack:
                    _logger.Info("Attack", "");
                    break;
                case TurnType.Block:
                    _logger.Info("Block", "");
                    break;
                case TurnType.Inventory:
                    _logger.Info("Inventory", "");
                    break;
                case TurnType.Spells:
                    _logger.Info("Spells", "");
                    break;
                case TurnType.Escape:
                    _logger.Info("Escape", "");
                    GuiController.PanelManager.OpenPanel(PanelType.MainMenu);
                    break;
            }
        }

        public List<int> DetermineTurnQueue(List<int> turnQueue, List<CharacterBase> characterList)
        {
            Random rand = new Random();
            int temp = rand.Next(0, characterList.Count);
            while (true)
            {
                if (!turnQueue.Contains(temp))
                {
                    turnQueue.Add(temp);
                    temp = rand.Next(0, characterList.Count);
                }
                if (turnQueue.Count == 3) break;
            }
            return turnQueue;
        }

        public void StartFight(List<CharacterBase> allyCharacterList, List<CharacterBase> enemyCharacterList)
        {
            List<int> allyTurnQueue = new List<int>();
            List<int> enemyTurnQueue = new List<int>();
            Random rand = new Random();
            int teamTurn = rand.Next(0, 2); // keeps the number that decide which team will start. 0 for ally, 1 for enemy.
            List<CharacterBase> allyListClone = allyCharacterList; // there is a doubt whether it's a deep copy or not.
            List<CharacterBase> enemyListClone = enemyCharacterList;


            allyTurnQueue = DetermineTurnQueue(allyTurnQueue, allyCharacterList);
            enemyTurnQueue = DetermineTurnQueue(enemyTurnQueue, enemyCharacterList);

            if (teamTurn == 0)
                Turn(allyTurnQueue, enemyTurnQueue, teamTurn);
            else
                Turn(enemyTurnQueue, allyTurnQueue, teamTurn);
        }

        private void Turn(List<int> startingTeamQueue, List<int> otherTeamQueue, int teamTurn)
        {
            if (teamTurn == 0)
            {
                // starting team is ally with the in order to startingTeamQueue list.
            }
            else if (teamTurn == 1)
            {
                // starting team is enemy in order to otherTeamQueue list.
            }
        }

        


        #region Singleton
        private static TurnBasedController _instance;
        public static TurnBasedController Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<TurnBasedController>();

                if (_instance != null)
                    return _instance;

                var obj = new GameObject("TurnBasedController");

                _instance = obj.AddComponent<TurnBasedController>();
                return _instance;
            }
        }
        #endregion

    }
}
