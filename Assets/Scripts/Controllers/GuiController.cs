﻿using Managers;
using Models.Enums;
using UnityEngine;

namespace Controllers
{
    public class GuiController : MonoBehaviour
    {
        private void Awake()
        {
            PanelManager.OpenPanel(PanelType.MainMenu);
        }

        #region Singleton and Manager Instances
        private static GuiController _instance;

        public static GuiController Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<GuiController>();
                if (_instance != null)
                    return _instance;

                var obj = new GameObject("GuiController");
                _instance = obj.AddComponent<GuiController>();
                return _instance;
            }
        }

        private static PanelManager _panelMananger;
        public static PanelManager PanelManager
        {
            get
            {
                if (_panelMananger == null)
                    _panelMananger = FindObjectOfType<PanelManager>();

                return _panelMananger;
            }
        }

        private static PopupManager _popupManager;
        public static PopupManager PopupManager
        {
            get
            {
                if (_popupManager == null)
                    _popupManager = FindObjectOfType<PopupManager>();

                return _popupManager;
            }
        }
        #endregion
    }
}
