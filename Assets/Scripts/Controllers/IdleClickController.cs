﻿using System.Collections;
using UnityEngine;

namespace Controllers
{
    public class IdleClickController : MonoBehaviour
    {

        public void StartCollecting()
        {
            PrepareOfflineReward();
            StartCoroutine(GeneratePerSecond());
        }

        public void StopCollecting()
        {
            //Get last collection time
            StopCoroutine(GeneratePerSecond());
        }

        private void PrepareOfflineReward()
        {

        }

        private IEnumerator GeneratePerSecond()
        {
            while (true)
            {
                //Calculate arcane point income per second
                yield return new WaitForSeconds(1f);
            }
        }

        #region Singleton
        private static IdleClickController _instance;
        public static IdleClickController Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<IdleClickController>();

                if (_instance != null)
                    return _instance;

                var obj = new GameObject("IdleClickController");

                _instance = obj.AddComponent<IdleClickController>();
                return _instance;
            }
        }
        #endregion
    }
}
