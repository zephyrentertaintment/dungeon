﻿using UnityEngine;

namespace Controllers
{
    public class GameController : MonoBehaviour
    {
        private TurnBasedController _turnBasedControllerInstance;
        
        public TurnBasedController TurnBasedControllerInstance
        {
            get { return _turnBasedControllerInstance; }
            private set { _turnBasedControllerInstance = value; }
        }

        private void Awake()
        {
            TurnBasedControllerInstance = TurnBasedController.Instance;
        }

        #region Singleton
        private static GameController _instance;
        public static GameController Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                _instance = FindObjectOfType<GameController>();

                if (_instance != null)
                    return _instance;

                var obj = new GameObject("GameController");

                _instance = obj.AddComponent<GameController>();
                return _instance;
            }
        }

        #endregion
        
    }
}
