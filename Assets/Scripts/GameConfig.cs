﻿using System.Collections.Generic;
using Models.Enums;
using Models.Spells;

public class GameConfig
{
    public Dictionary<MinionType, string> MonsterTypes;
    public Dictionary<SpellType, Spell> Spells;

    public GameConfig()
    {
        MonsterTypes = new Dictionary<MinionType, string>
        {
            { MinionType.Slime, "tobefixed" }
        };

        Spells = new Dictionary<SpellType, Spell>
        {
            { SpellType.FireBlast, new Spell(SpellType.FireBlast, 10.0, 10, 0, 5) },
            { SpellType.FireChamber, new Spell(SpellType.FireChamber, 20.0, 0, 2, 10, new List<Effect>
                {
                    new TemporaryDebuff(10, 2, TargetType.TargetEnemy)
                })
            },
            { SpellType.FireWave, new Spell(SpellType.FireWave, 30.0, 30, 3, 25) },
            { SpellType.FireBall, new Spell(SpellType.FireBall, 40.0, 100, 0, 75) },
            { SpellType.PyroBeam, new Spell(SpellType.PyroBeam, 100.0, 1000, 10, 250) },
            { SpellType.PassiveEffect, new Spell(SpellType.PassiveEffect, 20.0, 0, 0, 0, new List<Effect>
                {
                    new PermanentBuff(10, 2, TargetType.Self)
                })
            },
        };
    }
}
