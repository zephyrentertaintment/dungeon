﻿namespace Models.Enums
{
    public enum TargetType
    {
        None = 0,

        Self = 1,
        Party = 2,
        TargetEnemy = 3,
        AllEnemies = 4,
        Everyone = 5
    }
}