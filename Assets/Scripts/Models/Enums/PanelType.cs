﻿namespace Models.Enums
{
    public enum PanelType
    {
        MainMenu = 1,
        Credits = 2,

        IdleClick = 10,
        Dungeon = 11,
        Combat = 12,
        CharacterCreation = 13,
    }
}