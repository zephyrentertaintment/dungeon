﻿using System;

namespace Models.Enums
    {
        [Serializable]
        public enum TurnType {
            None = 0,
            Attack = 1,
            Block = 2,
            Inventory = 3,
            Spells = 4,
            Escape = 5
        }
    }
