﻿namespace Models.Enums
{
    public enum SpellType
    {
        None = 0,

        //Fire = 1-10
        FireBlast = 1,
        FireChamber = 2,
        FireWave = 3,
        FireBall = 4,
        PyroBeam = 5,
        ImmolationAura = 6,
        PhoenixBlood = 7,
        FlamingStrike = 8,
        PulseOfAzkanar = 9,
        PassiveEffect = 10,
        
        //Water = 11-20

        //Earth = 21-30
    }
}