﻿namespace Models.Character
{
    public class Character : CharacterBase {

        private string _name;

        public Character() {
            _name = null;
        }

        public Character(string name) {
            _name = name;
        }


        public void setName(string name) {
            _name = name;
        }

        public string getName() {
            return _name;
        }
    }
}
