﻿using System;
using UnityEngine;

namespace Models.Character
{
    public class CharacterBase : MonoBehaviour {

        private string _name;

        public event Action<String> SkillUsed;

        public void UseSkill() {
            if (SkillUsed != null) SkillUsed("Something");
        }

        public void setName(string newName) {
            _name = newName;
        }

        public string getName() {
            return _name;
        }



    }
}
