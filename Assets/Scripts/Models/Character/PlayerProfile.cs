﻿using UnityEngine;

namespace Models.Character
{
    [System.Serializable]
    public class PlayerProfile
    {
        private const double GoldenRatio = 1.61;

        //Level & xp
        public int Level;
        public int Experience;
        private int _xpReqForNextLevel;

        //Points for spell + stats, increase them whenever you earn a level or grant through other means.
        private int _statPoint;
        private int _arcaneKnowledge;

        #region Stats variables & properties
        private int _strength;
        private int _agility;
        private int _intelligence;

        private int _hitPoints;
        private int _powerPoints;

        private int _arcanePowerRegeneration;

        private double _criticalHitChance;
        private double _criticalHitDamage;
        private double _speedRating;
        private double _doubleAttackChance;
        private double _masteryRating;
        private double _arcanePowerRift;

        public int Intelligence
        {
            get { return _intelligence; }
        }

        public double CriticalHitChance
        {
            get { return _criticalHitChance; }
            private set { _criticalHitChance = value * GoldenRatio / 100; }
        }

        public double CriticalHitDamage
        {
            get { return _criticalHitDamage; }
            private set { _criticalHitDamage = value * GoldenRatio * 50; }
        }

        public double SpeedRating
        {
            get { return _speedRating; }
            private set { value = Intelligence * GoldenRatio / 100; }
        }

        public double DoubleAttackChance
        {
            get { return _doubleAttackChance; }
            private set { value = Intelligence * GoldenRatio / 100; }
        }

        public double MasteryRating
        {
            get { return _masteryRating; }
            private set { value = Intelligence * GoldenRatio / 100; }
        }

        public double ArcanePowerRift
        {
            get { return _arcanePowerRift; }
            private set { value = Intelligence * GoldenRatio / 100; }
        }

        public int ArcanePowerRegeneration
        {
            get { return _arcanePowerRegeneration; }
            private set { value = Intelligence; }
        } 
        #endregion

        public int XpReqForNextLevel
        {
            get { return _xpReqForNextLevel; }
            private set { _xpReqForNextLevel = Mathf.FloorToInt((float) (value * 150 * GoldenRatio)); }
        }

        public void IncreaseIntelligence()
        {

        }

        //For new creation!
        public PlayerProfile()
        {
            Level = 1;
            Experience = 0;

            _strength = 5;
            _agility = 5;
            _intelligence = 5;

            XpReqForNextLevel = Level;

            CriticalHitChance = Intelligence;
            CriticalHitDamage = Intelligence;
        }

        //For existing player savegame.
        public PlayerProfile(int level, int experience, int strength, int agility, int intelligence)
        {

        }
    }
}
