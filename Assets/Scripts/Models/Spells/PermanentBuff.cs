﻿using Models.Enums;

namespace Models.Spells
{
    public class PermanentBuff : Buff {

        public PermanentBuff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {

        }
    }
}
