﻿using Models.Enums;

namespace Models.Spells
{
    public class PermanentDebuff : Debuff {

        public PermanentDebuff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {

        }
    }
}
