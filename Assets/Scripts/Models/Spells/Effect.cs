﻿using Models.Enums;

namespace Models.Spells
{
    public class Effect
    {
        public int Value;
        public int Duration;
        public TargetType EffectTargetType;

        public Effect(int value, int duration, TargetType effectTargetType)
        {
            Value = value;
            Duration = duration;
            EffectTargetType = effectTargetType;
        }
    }
}
