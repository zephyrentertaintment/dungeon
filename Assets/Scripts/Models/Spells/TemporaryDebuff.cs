﻿using Models.Enums;

namespace Models.Spells
{
    public class TemporaryDebuff : Debuff {

        public TemporaryDebuff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {

        }
    }
}
