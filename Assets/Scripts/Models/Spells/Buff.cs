﻿using Models.Enums;

namespace Models.Spells
{
    public class Buff : Effect {

        public Buff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {

        }
    }
}
