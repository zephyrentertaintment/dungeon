﻿using Models.Enums;

namespace Models.Spells
{
    public class Debuff : Effect {

        public Debuff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {
            
        }
    }
}
