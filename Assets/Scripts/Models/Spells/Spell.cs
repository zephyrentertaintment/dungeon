﻿using System.Collections.Generic;
using Models.Enums;

namespace Models.Spells
{
    public class Spell
    {
        private SpellType _spellIdentifier;
        
        //required for idle click & research
        public double PassivePercentage;

        //required for dungeon crawler skill part
        public int ActiveSkillDamage;
        public int ActiveSkillCooldown;
        public double ActiveSkillCost;
        public TargetType TargetType;

        //required for both;
        private int _castCount;
        private int _experience;

        //Effects
        public List<Effect> BuffsAndDebuffs;

        public Spell(SpellType spellIdentifier, double passivePercentage, int activeSkillDamage, int activeSkillCooldown, double activeSkillCost, List<Effect> buffsAndDebuffs = null)
        {
            _spellIdentifier = spellIdentifier;
            PassivePercentage = passivePercentage;
            ActiveSkillDamage = activeSkillDamage;
            ActiveSkillCooldown = activeSkillCooldown;
            ActiveSkillCost = activeSkillCost;
            BuffsAndDebuffs = buffsAndDebuffs;
        }
    }
}
