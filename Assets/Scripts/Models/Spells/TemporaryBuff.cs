﻿using Models.Enums;

namespace Models.Spells
{
    public class TemporaryBuff : Buff {

        public TemporaryBuff(int value, int duration, TargetType effectTargetType) : base(value, duration, effectTargetType)
        {

        }
    }
}
