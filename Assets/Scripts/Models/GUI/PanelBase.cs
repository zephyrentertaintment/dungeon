﻿using Models.Enums;
using UnityEngine;

namespace Models.GUI
{
    public class PanelBase : MonoBehaviour
    {
        [SerializeField]
        protected PanelType PanelType;

        public PanelType Type
        {
            get { return PanelType; }
        }

        protected virtual void OnEnable()
        {
            Debug.Log("A panel's opening");
        }

        protected virtual void OnDisable()
        {
            Debug.Log("A panel's closing");
        }
    }
}
