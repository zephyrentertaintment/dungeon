﻿using Models.Enums;

namespace Models.Objects
{
    public class Tile
    {
        public TileType TileType;
        public int Column, Row;

        public Tile(TileType tileType, int column, int row)
        {
            TileType = tileType;
            Column = column;
            Row = row;
        }
    }
}
